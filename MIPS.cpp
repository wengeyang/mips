#include <cstdio>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <map>
#include <vector>
#include <string>
#include <fstream>
using namespace std;
map <string, int> line_label_num, data_label_num;
vector<string> pm, data;
vector<int> data_adress;
unsigned char memory[5000005];
int type[5000005];
int sp, fp, hi, lo, line_num = 0, data_num = 0, reg[40], data_space = 100;
string type_name[10] = {"byte", "halfword", "word", "char"};
string reg_name[40] = {"zero", "at", "v0", "v1", "a0", "a1", "a2", "a3", "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7", "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "t8", "t9", "k0", "k1", "gp", "sp", "fp", "ra", "hi", "lo", "pc"};
string program_operator[100] = {"add", "addu", "addiu", "sub", "subu", "mul", "mulu", "div", "divu", "xor", "xoru", "neg", "negu", "rem", "remu", "li", "seq", "sge", "sgt", "sle", "slt", "sne", "b", "beq", "bne", "bge", "ble", "bgt", "blt", "beqz", "bnez", "blez", "bgez", "bgtz", "bltz", "j", "jr", "jal", "jalr", "la", "lb", "lh", "lw", "sb", "sh", "sw", "move", "mfhi", "mflo", "nop", "syscall"};
struct text
{
	int type;
	int num1, num2, num3, p;
	bool is_num1, is_num2, is_num3;
}program[1000005];
int op_type(string s)
{
	for (int i = 0; i < 100; i++)
		if (s == program_operator[i])
			return i;
	return -1;
}
inline int type_name_num (string s)
{
	for (int i = 0; i <= 3; i++)
		if (s == type_name[i])
			return i;
	return -1;
}
void del_space(string &s)
{
	while (s.find(" ") == 0 || s.find("\t") == 0)
	{
		s = s.substr(1);
	}
}
int string_num(string s)
{
	if (s == "")
		return 0;
	int n = s.length(), num = 0;
	if (s[0] != '-')
	{
		for (int i = 0; i < n; i++)
		{
			num = num * 10 + s[i] - '0';
		}
		return num;
	}
	else
	{
		for (int i = 1; i < n; i++)
		{
			num = num * 10 + s[i] - '0';
		}
		return -1 * num;
	}
}
bool is_string_num (string s)
{
	int n = s.length();
	if (s[0] != '-')
	{
		for (int i = 0; i < n; i++)
			if (s[i] < '0' || s[i] > '9')
				return false;
	}
	else
	{
		for (int i = 1; i < n; i++)
			if (s[i] < '0' || s[i] > '9')
				return false;
	}
	return true;
}
int get_reg_num (string s)
{
	if (is_string_num(s))
		return string_num(s);
	for (int i = 0; i <= 34; i++)
		if (s == reg_name[i])
			return i;
	return -1;
}
int get_char_num (string &s, char a, int x = 0)
{
	int n = s.length();
	for (int i = x; i < n; i++)
		if (s[i] == a)
			return i;
	return -1;
}
int string_to_address(string s)
{
	int rt = 0;
	if (s.find("$") == 0)
	{
		s = s.substr(1);
		rt = get_reg_num(s);
	}
	else
	{
		rt = data_adress[data_label_num[s]];
	}
	return rt;
}
int label_to_num (int x)
{
	int num = 0;
	num = memory[x];
	if (type[x] == 3)
		return num;
	if (type[x] > 0)
	{
		num <<= 8;
		num += memory[x + 1];
	}
	if (type[x] > 2)
	{
		num <<= 8;
		num += memory[x + 2];
		num <<= 8;
		num += memory[x + 3];
	}
	return num;
}
int get_in (string &s, ifstream &infile)
{
	if (!getline (infile, s))
	{
		if (!infile.eof())
		{
			return 0;
		}
		return -1;
	}
	int n = s.length();
	if (n == 0)
	{
		return 0;
	}
	if (s[0] == '#')
	{
		return 0;
	}
	int x = s.find('#');
	if (x != -1)
	{
		s = s.substr(0, x);
	}
	x = s.find('.');
	if (x != -1)
	{
		s = s.substr(x + 1);
		return 1;
	}
	x = s.find(':');
	if (x != -1)
	{
		s = s.substr(0, x);
		return 2;
	}
	return 3;
}
void add (int &ad1, int ad2, int ad3)
{
	ad1 = ad2 + ad3;
}
void sub (int &ad1, int ad2, int ad3)
{
	ad1 = ad2 - ad3;
}
int mul (int &ans, int a1, int a2)
{
	ans = a1 * a2;
	long long p;
	p = a1 * a2;
	return int (p >> 32);
}
int div (int &ans, int a1, int a2)
{
	ans = a1 / a2;
	return a1 % a2;
}
void xr (int &ans, int a1, int a2)
{
	ans = a1 ^ a2;
}
void rem (int &ans, int a1, int a2)
{
	ans = a1 % a2;
}
void load (int &ad1, int ad2, int len = 0)
{
	if (len == 0)
	{
		ad1 = ad2;
	}
	else
	{
		int num = 0;
		for (int i = 0; i < len; i++)
			num = (num << 8) + memory[ad2 + i];
		ad1 = num;
	}
	return;
}
void store (int ad1, int ad2, int len)
{
	unsigned int x = ad1;
	if (len == 4)
	{
		memory[ad2] = x >> 24;
		memory[ad2 + 1] = x >> 16;
		memory[ad2 + 2] = x >> 8;
		memory[ad2 + 3] = x;
	}
	else if (len == 2)
	{
		memory[ad2] = x >> 8;
		memory[ad2 + 1] = x;
	}
	else
	{
		memory[ad2] = x;
	}
}
void system_call(int q, int num)
{
	if (num == 1)
	{
		cout << q;
	}
	else if (num == 4)
	{
		int n = q;
		while (memory[n] != '\0')
		{
			cout << (char) memory[n];
			n++;
		}
	}
	else if (num == 5)
	{
		cin >> reg[get_reg_num("v0")];
	}
	else if (num == 8)
	{
		string now;
		cin >> now;
		int n = now.length();
		int x = q;
		for (int i = 0; i < n; i++)
		{
			memory[x] = now[i];
			type[x] = type_name_num("char");
			x++;
		}
		memory[x] = '\0';
		type[x] = type_name_num("char");
	}
	else if (num == 9)
	{
		reg[get_reg_num("v0")] = data_space;
		if (q % 4)
			q += 4 - q % 4;
		data_space += q;
	}
	else if (num == 10)
	{
		exit(0);
	}
	else if (num == 17)
	{
		exit(q);
	}
}
void Init (const char *argc)
{
	ifstream in;
	in.open (argc, ios::in);
	string s;
	int style;
	bool is_data = false;
	reg[get_reg_num("sp")] = 4500000;
	style = get_in(s, in);
	while (style != -1)
	{
		del_space(s);
		if (style == 1)
		{
			if (s == "data")
			{
				is_data = true;
			}
			else if (s == "text")
			{
				is_data = false;
			}
			else
			{
				data.push_back(s);
				data_num++;
			}
		}
		else if (style == 2)
		{
			if (is_data)
			{
				data_label_num[s] = data_num;
			}
			else
			{
				line_label_num[s] = line_num;
			}
		}
		else if (style == 3)
		{
			pm.push_back(s);
			line_num++;
		}
		style = get_in(s, in);
	}
}
void build_data()
{
	for (int line = 0; line < data_num; line++)
	{
		string now = data[line];
		data_adress.push_back(data_space);
		string op = now.substr(0, now.find(" "));
		now = now.substr(now.find(" ") + 1);
		if (op == "align")
		{
			int n = string_num (now);
			int x = 1 << n;
			if (data_space % x)
				data_space += x - data_space % x;
		}
		else if (op == "ascii")
		{
			int n = now.length();
			now = now.substr(1, n - 2);
			n = now.length();
			for (int i = 0; i < n; i++)
			{
				if (now[i] == '\\')
				{
					if (now[i + 1] == '\\' || now[i + 1] == '\'' || now[i + 1] == '\"' || now[i + 1] == '\?')
						i++;
					else if (now[i + 1] == '0')
					{
						now[i + 1] = '\0';
						i++;
					}
					else if (now[i + 1] == 'n')
					{
						now[i + 1] = '\n';
						i++;
					}
					else if (now[i + 1] == 't')
					{
						now[i + 1] = '\t';
						i++;
					}
					else if (now[i + 1] == 'b')
					{
						now[i + 1] = '\b';
						i++;
					}
					else if (now[i + 1] == 'f')
					{
						now[i + 1] = '\f';
						i++;
					}
				}
				memory[data_space] = now[i];
				type[data_space] = type_name_num("char");
				data_space++;
			}
		}
		else if (op == "asciiz")
		{
			int n = now.length();
			now = now.substr(1, n - 2);
			n = now.length();
			for (int i = 0; i < n; i++)
			{if (now[i] == '\\')
				{
					if (now[i + 1] == '\\' || now[i + 1] == '\'' || now[i + 1] == '\"' || now[i + 1] == '\?')
						i++;
					else if (now[i + 1] == '0')
					{
						now[i + 1] = '\0';
						i++;
					}
					else if (now[i + 1] == 'n')
					{
						now[i + 1] = '\n';
						i++;
					}
					else if (now[i + 1] == 't')
					{
						now[i + 1] = '\t';
						i++;
					}
					else if (now[i + 1] == 'b')
					{
						now[i + 1] = '\b';
						i++;
					}
					else if (now[i + 1] == 'f')
					{
						now[i + 1] = '\f';
						i++;
					}
				}
				memory[data_space] = now[i];
				type[data_space] = type_name_num("char");
				data_space++;
			}
			memory[data_space] = '\0';
			type[data_space] = type_name_num("char");
			data_space++;
		}
		else if (op == "byte")
		{
			int x = now.find(",");
			while (x != -1)
			{
				string q = now.substr(0, x);
				now = now.substr(x + 1);
				int n = string_num(q);
				type[data_space] = type_name_num("byte");
				memory[data_space] = n;
				data_space++;
				x = now.find(",");
			}
			int n = string_num(now);
			type[data_space] = type_name_num("byte");
			memory[data_space] = n;
			data_space++;
		}
		else if (op == "halfword")
		{
			int x = now.find(",");
			while (x != -1)
			{
				string q = now.substr(0, x);
				now = now.substr(x + 1);
				int n = string_num(q);
				type[data_space] = type_name_num("halfword");
				memory[data_space + 1] = n;
				memory[data_space] = n >> 8;
				data_space += 2;
				x = now.find(",");
			}
			int n = string_num(now);
			type[data_space] = type_name_num("halfword");
			memory[data_space + 1] = n;
			memory[data_space] = n >> 8;
			data_space += 2;
		}
		else if (op == "word")
		{
			int x = now.find(",");
			while (x != -1)
			{
				string q = now.substr(0, x);
				now = now.substr(x + 1);
				int n = string_num(q);
				type[data_space] = type_name_num("word");
				memory[data_space + 3] = n ;
				memory[data_space + 2] = n >> 8;
				memory[data_space + 1] = n >> 16;
				memory[data_space] = n >> 24;
				data_space += 4;
				x = now.find(",");
			}
			int n = string_num(now);
			type[data_space] = type_name_num("halfword");
			memory[data_space + 3] = n ;
			memory[data_space + 2] = n >> 8;
			memory[data_space + 1] = n >> 16;
			memory[data_space] = n >> 24;
			data_space += 4;
		}
		else if (op == "space")
		{
			int n = string_num(now);
			data_space += n;
		}
	}
	sp = data_space - 1;
}
void work ()
{
	int i = line_label_num["main"];
	while (i < line_num)
	{
		if (program[i].type < 3)
		{
			int rt, rc1, sc2;
			rt = program[i].num1;
			rc1 = program[i].num2;
			sc2 = program[i].num3;
			if (program[i].is_num3)
			{
				add (reg[rt], reg[rc1], sc2);
			}
			else
			{
				if (sc2 < 100)
					add (reg[rt], reg[rc1], reg[sc2]);
				else
					add (reg[rt], reg[rc1], label_to_num(sc2));
			}
		}
		else if (program[i].type < 5)
		{
			int rt, rc1, sc2;
			rt = program[i].num1;
			rc1 = program[i].num2;
			sc2 = program[i].num3;
			if (program[i].is_num3)
			{
				sub (reg[rt], reg[rc1], sc2);
			}
			else
			{
				if (sc2 < 100)
					sub (reg[rt], reg[rc1], reg[sc2]);
				else
					sub (reg[rt], reg[rc1], label_to_num(sc2));
			}
		}
		else if (program[i].type < 7)
		{
			int rt, rc1, sc2;
			rt = program[i].num1;
			rc1 = program[i].num2;
			sc2 = program[i].num3;
			if (sc2 == 0)
			{
				if (program[i].is_num2)
				{
					reg[get_reg_num("hi")] = mul(reg[get_reg_num("lo")],reg[rt],rc1);
				}
				else
				{
					if (rc1 < 100)
						reg[get_reg_num("hi")] = mul(reg[get_reg_num("lo")],reg[rt],reg[rc1]);
					else
						reg[get_reg_num("hi")] = mul(reg[get_reg_num("lo")],reg[rt],label_to_num(rc1));
				}
			}
			else
			{
				if (program[i].is_num3)
				{
					mul (reg[rt], reg[rc1], sc2);
				}
				else
				{
					if (sc2 < 100)
						mul (reg[rt], reg[rc1], reg[sc2]);
					else
						mul (reg[rt], reg[rc1], label_to_num(sc2));
				}
			}
		}
		else if (program[i].type < 9)
		{
			int rt, rc1, sc2;
			rt = program[i].num1;
			rc1 = program[i].num2;
			sc2 = program[i].num3;
			if (sc2 == 0)
			{
				if (program[i].is_num2)
				{
					reg[get_reg_num("hi")] = div(reg[get_reg_num("lo")],reg[rt],rc1);
				}
				else
				{
					if (rc1 < 100)
						reg[get_reg_num("hi")] = div(reg[get_reg_num("lo")],reg[rt],reg[rc1]);
					else
						reg[get_reg_num("hi")] = div(reg[get_reg_num("lo")],reg[rt],label_to_num(rc1));
				}
			}
			else
			{
				if (program[i].is_num3)
				{
					div (reg[rt], reg[rc1], sc2);
				}
				else
				{
					if (sc2 < 100)
						div (reg[rt], reg[rc1], reg[sc2]);
					else
						div (reg[rt], reg[rc1], label_to_num(sc2));
				}
			}
		}
		else if (program[i].type < 11)
		{
			int rt, rc1, sc2;
			rt = program[i].num1;
			rc1 = program[i].num2;
			sc2 = program[i].num3;
			if (program[i].is_num3)
			{
				xr (reg[rt], reg[rc1], sc2);
			}
			else
			{
				if (sc2 < 100)
					xr (reg[rt], reg[rc1], reg[sc2]);
				else
					xr (reg[rt], reg[rc1], label_to_num(sc2));
			}
		}
		else if (program[i].type < 13)
		{
			int rt, rc1;
			rt = program[i].num1;
			rc1 = program[i].num2;
			if (program[i].type == 11)
			{
				reg[rt] = -reg[rc1];
			}
			else
			{
				reg[rt] = ~reg[rc1];
			}
		}
		else if (program[i].type < 15)
		{
			int rt, rc1, sc2;
			rt = program[i].num1;
			rc1 = program[i].num2;
			sc2 = program[i].num3;
			if (program[i].is_num3)
			{
				rem (reg[rt], reg[rc1], sc2);
			}
			else
			{
				if (sc2 < 100)
					rem (reg[rt], reg[rc1], reg[sc2]);
				else
					rem (reg[rt], reg[rc1], label_to_num(sc2));
			}
		}
		else if (program[i].type < 16)
		{
			int rt, rc1;
			rt = program[i].num1;
			rc1 = program[i].num2;
			reg[rt] = rc1;
		}
		else if (program[i].type < 22)
		{
			int rt, rc1, sc2;
			rt = program[i].num1;
			rc1 = program[i].num2;
			sc2 = program[i].num3;
			if (!program[i].is_num3)
			{
				sc2 = reg[sc2];
			}
			if (program[i].type == 16)
			{
				reg[rt] = (reg[rc1] == sc2);
			}
			else if (program[i].type == 17)
			{
				reg[rt] = (reg[rc1] >= sc2);
			}
			else if (program[i].type == 18)
			{
				reg[rt] = (reg[rc1] > sc2);
			}
			else if (program[i].type == 19)
			{
				reg[rt] = (reg[rc1] <= sc2);
			}
			else if (program[i].type == 20)
			{
				reg[rt] = (reg[rc1] < sc2);
			}
			else
			{
				reg[rt] = (reg[rc1] != sc2);
			}
		}
		else if (program[i].type < 23)
		{
			i = program[i].num1 - 1;
		}
		else if (program[i].type < 29)
		{
			int rt, rc1;
			rt = program[i].num1;
			rc1 = program[i].num2;
			if (!program[i].is_num2)
			{
				rc1 = reg[rc1];
			}
			if (program[i].type == 23)
			{
				if (reg[rt] == rc1)
					i = program[i].num3 - 1;
			}
			else if (program[i].type == 24)
			{
				if (reg[rt] != rc1)
					i = program[i].num3 - 1;
			}
			else if (program[i].type == 25)
			{
				if (reg[rt] >= rc1)
					i = program[i].num3 - 1;
			}
			else if (program[i].type == 26)
			{
				if (reg[rt] <= rc1)
					i = program[i].num3 - 1;
			}
			else if (program[i].type == 27)
			{
				if (reg[rt] > rc1)
					i = program[i].num3 - 1;
			}
			else
			{
				if (reg[rt] < rc1)
					i = program[i].num3 - 1;
			}
		}
		else if (program[i].type < 35)
		{
			int rt, rc1;
			rt = program[i].num1;
			rc1 = program[i].num2;
			rt = reg[rt];
			if (program[i].type == 29)
			{
				if (rt == 0)
					i = rc1 - 1;
			}
			else if (program[i].type == 30)
			{
				if (rt != 0)
					i = rc1 - 1;
			}
			else if (program[i].type == 31)
			{
				if (rt <= 0)
					i = rc1 - 1;
			}
			else if (program[i].type == 32)
			{
				if (rt >= 0)
					i = rc1 - 1;
			}
			else if (program[i].type == 33)
			{
				if (rt > 0)
					i = rc1 - 1;
			}
			else
			{
				if (rt < 0)
					i = rc1 - 1;
			}
		}
		else if (program[i].type < 39)
		{
			if (program[i].type == 35 || program[i].type == 37)
			{
				if (program[i].type == 37)
					reg[31] = i + 1;
				i = program[i].num1 - 1;
			}
			else
			{
				if (program[i].type == 38)
					reg[31] = i + 1;
				i = reg[program[i].num1] - 1;
			}
		}
		else if (program[i].type < 43)
		{
			int rc, adr, adp;
			rc = program[i].num1;
			adr = program[i].num2;
			adp = program[i].p;
			if (program[i].type == 39)
			{
				reg[rc] = adr + adp;
			}
			else if (program[i].type == 40)
			{
				if (adr >= 100)
					load (reg[rc], adr + adp, 1);
				else
					load (reg[rc], reg[adr] + adp, 1);
			}
			else if (program[i].type == 41)
			{
				if (adr >= 100)
					load (reg[rc], adr + adp, 2);
				else
					load (reg[rc], reg[adr] + adp, 2);
			}
			else
			{
				if (adr >= 100)
					load (reg[rc], adr + adp, 4);
				else
					load (reg[rc], reg[adr] + adp, 4);
			}
		}
		else if (program[i].type < 46)
		{
			int rc, adr, adp;
			rc = program[i].num1;
			adr = program[i].num2;
			adp = program[i].p;
			if (program[i].type == 43)
			{
				if (adr >= 100)
					store (reg[rc], adr + adp, 1);
				else
					store (reg[rc], reg[adr] + adp, 1);
			}
			else if (program[i].type == 44)
			{
				if (adr >= 100)
					store (reg[rc], adr + adp, 2);
				else
					store (reg[rc], reg[adr] + adp, 2);
			}
			else
			{
				if (adr >= 100)
					store (reg[rc], adr + adp, 4);
				else
					store (reg[rc], reg[adr] + adp, 4);
			}
		}
		else if (program[i].type < 49)
		{
			if (program[i].type == 46)
			{
				int rt, rc;
				rt = program[i].num1;
				rc = program[i].num2;
				if (rt != rc)
					reg[rt] = reg[rc];
			}
			if (program[i].type == 47)
			{
				int rt;
				rt = program[i].num1;
				reg[rt] = reg[get_reg_num("hi")];
			}
			if (program[i].type == 48)
			{
				int rt;
				rt = program[i].num1;
				reg[rt] = reg[get_reg_num("lo")];
			}
		}
		else if (program[i].type == 50)
		{
			system_call(reg[get_reg_num("a0")], reg[get_reg_num("v0")]);
		}
		i++;
	}
}
void build_program()
{
	for (int i = 0; i < line_num; i++)
	{
		string now = pm[i], op;
		op = now.substr(0, now.find(" "));
		program[i].type = op_type(op);
		now = now.substr(now.find(" ") + 1);
		if (op[0] == 'b' || op == "j" || op == "jal")
		{
			if ((int)now.find(",") != -1)
			{
				string num = now.substr(0, now.find(","));
				now = now.substr(now.find(",") + 1);
				del_space(now);
				del_space(num);
				program[i].is_num1 = false;
				program[i].num1 = string_to_address(num);
				if ((int)now.find(",") != -1)
				{
					num = now.substr(0, now.find(","));
					now = now.substr(now.find(",") + 1);
					del_space(now);
					del_space(num);
					if (is_string_num(num))
					{
						program[i].is_num2 = true;
						program[i].num2 = string_num(num);
					}
					else
					{
						program[i].is_num2 = false;
						program[i].num2 = string_to_address(num);
					}
					num = now;
					del_space(num);
					program[i].is_num3 = false;
					program[i].num3 = line_label_num[num];
				}
				else
				{
					num = now;
					del_space(num);
					program[i].is_num2 = false;
					program[i].num2 = line_label_num[num];
				}
			}
			else
			{
				string num = now;
				del_space(num);
				program[i].is_num1 = false;
				program[i].num1 = line_label_num[num];
			}
		}
		else
		{
			if ((int)now.find(",") != -1)
			{
				string num = now.substr(0, now.find(","));
				now = now.substr(now.find(",") + 1);
				del_space(now);
				del_space(num);
				if (is_string_num(num))
				{
					program[i].is_num1 = true;
					program[i].num1 = string_num(num);
				}
				else
				{
					program[i].is_num1 = false;
					program[i].num1 = string_to_address(num);
				}
				if ((int)now.find(",") != -1)
				{
					num = now.substr(0, now.find(","));
					now = now.substr(now.find(",") + 1);
					del_space(now);
					del_space(num);
					if (is_string_num(num))
					{
						program[i].is_num2 = true;
						program[i].num2 = string_num(num);
					}
					else
					{
						program[i].is_num2 = false;
						program[i].num2 = string_to_address(num);
					}
					num = now;
					del_space(num);
					if (is_string_num(num))
					{
						program[i].is_num3 = true;
						program[i].num3 = string_num(num);
					}
					else
					{
						program[i].is_num3 = false;
						program[i].num3 = string_to_address(num);
					}
				}
				else
				{
					num = now;
					del_space(num);
					if (is_string_num(num))
					{
						program[i].is_num2 = true;
						program[i].num2 = string_num(num);
					}
					else if ((int)num.find("(") == -1)
					{
						program[i].is_num2 = false;
						program[i].num2 = string_to_address(num);
					}
					else
					{
						del_space(now);
						num = now.substr(0, now.find("("));
						program[i].p = string_num(num);
						num = now.substr(now.find("(") + 1);
						num = num.substr(0, num.find(")"));
						program[i].is_num2 = false;
						program[i].num2 = string_to_address(num);
					}
				}
			}
			else
			{
				string num = now;
				del_space(num);
				if (is_string_num(num))
				{
					program[i].is_num1 = true;
					program[i].num1 = string_num(num);
				}
				else
				{
					program[i].is_num1 = false;
					program[i].num1 = string_to_address(num);
				}
			}
		}
	}
}
int main(int argc, char const *argv[])
{
	Init(argv[1]);
	build_data();
	build_program();
	work();
	return 0;
}