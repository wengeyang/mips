#include <cstdio>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <map>
#include <vector>
#include <string>
#include <fstream>
using namespace std;
map <string, int> line_label_num, data_label_num;
vector<string> pm, data;
vector<int> data_adress;
unsigned char memory[5000005];
int type[5000005];
ofstream out;
int sp, fp, hi, lo, line_num = 0, data_num = 0, reg[40], data_space = 100, pc;
string type_name[10] = {"byte", "halfword", "word", "char"};
string reg_name[40] = {"zero", "at", "v0", "v1", "a0", "a1", "a2", "a3", "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7", "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "t8", "t9", "k0", "k1", "gp", "sp", "fp", "ra", "hi", "lo", "pc"};
string program_operator[100] = {"add", "addu", "addiu", "sub", "subu", "mul", "mulu", "div", "divu", "xor", "xoru", "neg", "negu", "rem", "remu", "li", "seq", "sge", "sgt", "sle", "slt", "sne", "b", "beq", "bne", "bge", "ble", "bgt", "blt", "beqz", "bnez", "blez", "bgez", "bgtz", "bltz", "j", "jr", "jal", "jalr", "la", "lb", "lh", "lw", "sb", "sh", "sw", "move", "mfhi", "mflo", "nop", "syscall"};
struct text
{
	int type;
	int num1, num2, num3, p;
	bool is_num1, is_num2, is_num3;
}program[1000005];
bool lock_mem, lock_reg[105];
int op_type(string s)
{
	for (int i = 0; i < 100; i++)
		if (s == program_operator[i])
			return i;
	return -1;
}
inline int type_name_num (string s)
{
	for (int i = 0; i <= 3; i++)
		if (s == type_name[i])
			return i;
	return -1;
}
void del_space(string &s)
{
	while (s.find(" ") == 0 || s.find("\t") == 0)
	{
		s = s.substr(1);
	}
}
int string_num(string s)
{
	if (s == "")
		return 0;
	int n = s.length(), num = 0;
	if (s[0] != '-')
	{
		for (int i = 0; i < n; i++)
		{
			num = num * 10 + s[i] - '0';
		}
		return num;
	}
	else
	{
		for (int i = 1; i < n; i++)
		{
			num = num * 10 + s[i] - '0';
		}
		return -1 * num;
	}
}
bool is_string_num (string s)
{
	int n = s.length();
	if (s[0] != '-')
	{
		for (int i = 0; i < n; i++)
			if (s[i] < '0' || s[i] > '9')
				return false;
	}
	else
	{
		for (int i = 1; i < n; i++)
			if (s[i] < '0' || s[i] > '9')
				return false;
	}
	return true;
}
int get_reg_num (string s)
{
	if (is_string_num(s))
		return string_num(s);
	for (int i = 0; i <= 34; i++)
		if (s == reg_name[i])
			return i;
	return -1;
}
int get_char_num (string &s, char a, int x = 0)
{
	int n = s.length();
	for (int i = x; i < n; i++)
		if (s[i] == a)
			return i;
	return -1;
}
int string_to_address(string s)
{
	int rt = 0;
	if (s.find("$") == 0)
	{
		s = s.substr(1);
		rt = get_reg_num(s);
	}
	else
	{
		rt = data_adress[data_label_num[s]];
	}
	return rt;
}
int label_to_num (int x)
{
	int num = 0;
	num = memory[x];
	if (type[x] == 3)
		return num;
	if (type[x] > 0)
	{
		num <<= 8;
		num += memory[x + 1];
	}
	if (type[x] > 2)
	{
		num <<= 8;
		num += memory[x + 2];
		num <<= 8;
		num += memory[x + 3];
	}
	return num;
}
int get_in (string &s, ifstream &infile)
{
	if (!getline (infile, s))
	{
		if (!infile.eof())
		{
			return 0;
		}
		return -1;
	}
	int n = s.length();
	if (n == 0)
	{
		return 0;
	}
	if (s[0] == '#')
	{
		return 0;
	}
	int x = s.find('#');
	if (x != -1)
	{
		s = s.substr(0, x);
	}
	x = s.find('.');
	if (x != -1)
	{
		s = s.substr(x + 1);
		return 1;
	}
	x = s.find(':');
	if (x != -1)
	{
		s = s.substr(0, x);
		return 2;
	}
	return 3;
}
int load (int ad2, int len = 0)
{
	if (len == 0)
	{
		return ad2;
	}
	else
	{
		int num = 0;
		for (int i = 0; i < len; i++)
		{
			num = (num << 8) + memory[ad2 + i];
//			cout << num << endl;
		}
		return num;
	}
}
void store (int ad1, int ad2, int len)
{
	unsigned int x = ad1;
//	cout << x << endl;
	if (len == 4)
	{
		memory[ad2] = x >> 24;
		memory[ad2 + 1] = x >> 16;
		memory[ad2 + 2] = x >> 8;
		memory[ad2 + 3] = x;
	}
	else if (len == 2)
	{
		memory[ad2] = x >> 8;
		memory[ad2 + 1] = x;
	}
	else
	{
		memory[ad2] = x;
	}
}
void system_call(int q, int num)
{
	if (num == 1)
	{
		cout << q;
	}
	else if (num == 4)
	{
		int n = q;
		while (memory[n] != '\0')
		{
			cout << (char) memory[n];
			n++;
		}
	}
	else if (num == 5)
	{
		cin >> reg[get_reg_num("v0")];
	}
	else if (num == 8)
	{
		string now;
		cin >> now;
		int n = now.length();
		int x = q;
		for (int i = 0; i < n; i++)
		{
			memory[x] = now[i];
			type[x] = type_name_num("char");
			x++;
		}
		memory[x] = '\0';
		type[x] = type_name_num("char");
	}
	else if (num == 9)
	{
		reg[get_reg_num("v0")] = data_space;
		if (q % 4)
			q += 4 - q % 4;
		data_space += q;
	}
	else if (num == 10)
	{
		exit(0);
	}
	else if (num == 17)
	{
		exit(q);
	}
}
void Init (const char *argv)
{
	ifstream in;
	in.open (argv, ios::in);
	string s;
	int style;
	bool is_data = false;
	reg[get_reg_num("sp")] = 4500000;
	style = get_in(s, in);
	while (style != -1)
	{
		del_space(s);
		if (style == 1)
		{
			if (s == "data")
			{
				is_data = true;
			}
			else if (s == "text")
			{
				is_data = false;
			}
			else
			{
				data.push_back(s);
				data_num++;
			}
		}
		else if (style == 2)
		{
			if (is_data)
			{
				data_label_num[s] = data_num;
			}
			else
			{
				line_label_num[s] = line_num;
			}
		}
		else if (style == 3)
		{
			pm.push_back(s);
			line_num++;
		}
		style = get_in(s, in);
	}
}
void Init ()
{
	ifstream in;
	in.open ("1.s", ios::in);
	string s;
	int style;
	bool is_data = false;
	reg[get_reg_num("sp")] = 4500000;
	style = get_in(s, in);
	while (style != -1)
	{
		del_space(s);
		if (style == 1)
		{
			if (s == "data")
			{
				is_data = true;
			}
			else if (s == "text")
			{
				is_data = false;
			}
			else
			{
				data.push_back(s);
				data_num++;
			}
		}
		else if (style == 2)
		{
			if (is_data)
			{
				data_label_num[s] = data_num;
			}
			else
			{
				line_label_num[s] = line_num;
			}
		}
		else if (style == 3)
		{
			pm.push_back(s);
			line_num++;
		}
		style = get_in(s, in);
	}
}
void build_data()
{
	for (int line = 0; line < data_num; line++)
	{
		string now = data[line];
		data_adress.push_back(data_space);
		string op = now.substr(0, now.find(" "));
		now = now.substr(now.find(" ") + 1);
		if (op == "align")
		{
			int n = string_num (now);
			int x = 1 << n;
			if (data_space % x)
				data_space += x - data_space % x;
		}
		else if (op == "ascii")
		{
			int n = now.length();
			now = now.substr(1, n - 2);
			n = now.length();
			for (int i = 0; i < n; i++)
			{
				if (now[i] == '\\')
				{
					if (now[i + 1] == '\\' || now[i + 1] == '\'' || now[i + 1] == '\"' || now[i + 1] == '\?')
						i++;
					else if (now[i + 1] == '0')
					{
						now[i + 1] = '\0';
						i++;
					}
					else if (now[i + 1] == 'n')
					{
						now[i + 1] = '\n';
						i++;
					}
					else if (now[i + 1] == 't')
					{
						now[i + 1] = '\t';
						i++;
					}
					else if (now[i + 1] == 'b')
					{
						now[i + 1] = '\b';
						i++;
					}
					else if (now[i + 1] == 'f')
					{
						now[i + 1] = '\f';
						i++;
					}
				}
				memory[data_space] = now[i];
				type[data_space] = type_name_num("char");
				data_space++;
			}
		}
		else if (op == "asciiz")
		{
			int n = now.length();
			now = now.substr(1, n - 2);
			n = now.length();
			for (int i = 0; i < n; i++)
			{if (now[i] == '\\')
				{
					if (now[i + 1] == '\\' || now[i + 1] == '\'' || now[i + 1] == '\"' || now[i + 1] == '\?')
						i++;
					else if (now[i + 1] == '0')
					{
						now[i + 1] = '\0';
						i++;
					}
					else if (now[i + 1] == 'n')
					{
						now[i + 1] = '\n';
						i++;
					}
					else if (now[i + 1] == 't')
					{
						now[i + 1] = '\t';
						i++;
					}
					else if (now[i + 1] == 'b')
					{
						now[i + 1] = '\b';
						i++;
					}
					else if (now[i + 1] == 'f')
					{
						now[i + 1] = '\f';
						i++;
					}
				}
				memory[data_space] = now[i];
				type[data_space] = type_name_num("char");
				data_space++;
			}
			memory[data_space] = '\0';
			type[data_space] = type_name_num("char");
			data_space++;
		}
		else if (op == "byte")
		{
			int x = now.find(",");
			while (x != -1)
			{
				string q = now.substr(0, x);
				now = now.substr(x + 1);
				int n = string_num(q);
				type[data_space] = type_name_num("byte");
				memory[data_space] = n;
				data_space++;
				x = now.find(",");
			}
			int n = string_num(now);
			type[data_space] = type_name_num("byte");
			memory[data_space] = n;
			data_space++;
		}
		else if (op == "halfword")
		{
			int x = now.find(",");
			while (x != -1)
			{
				string q = now.substr(0, x);
				now = now.substr(x + 1);
				int n = string_num(q);
				type[data_space] = type_name_num("halfword");
				memory[data_space + 1] = n;
				memory[data_space] = n >> 8;
				data_space += 2;
				x = now.find(",");
			}
			int n = string_num(now);
			type[data_space] = type_name_num("halfword");
			memory[data_space + 1] = n;
			memory[data_space] = n >> 8;
			data_space += 2;
		}
		else if (op == "word")
		{
			int x = now.find(",");
			while (x != -1)
			{
				string q = now.substr(0, x);
				now = now.substr(x + 1);
				int n = string_num(q);
				type[data_space] = type_name_num("word");
				memory[data_space + 3] = n ;
				memory[data_space + 2] = n >> 8;
				memory[data_space + 1] = n >> 16;
				memory[data_space] = n >> 24;
				data_space += 4;
				x = now.find(",");
			}
			int n = string_num(now);
			type[data_space] = type_name_num("halfword");
			memory[data_space + 3] = n ;
			memory[data_space + 2] = n >> 8;
			memory[data_space + 1] = n >> 16;
			memory[data_space] = n >> 24;
			data_space += 4;
		}
		else if (op == "space")
		{
			int n = string_num(now);
			data_space += n;
		}
	}
	sp = data_space - 1;
}
int now_branch[1005], now_branch_num;
bool branch()
{
	return now_branch[now_branch_num] > 1;
}
void change_branch(bool q)
{
	if (q)
	{
		now_branch[now_branch_num]++;
		if (now_branch[now_branch_num] >= 4)
			now_branch[now_branch_num] = 4;
		now_branch_num <<= 1;
		now_branch_num += 1;
		now_branch_num %= 64;
	}
	else
	{
		now_branch[now_branch_num]--;
		if (now_branch[now_branch_num] < 0)
			now_branch[now_branch_num] = 0;
		now_branch_num <<= 1;
		now_branch_num %= 64;
	}
}
class cpu
{
public:
	int type, line;
	int num1, num2, num3, p;
	bool is_num1, is_num2, is_num3;
 	void clear()
 	{
 		type = -1;
 		line = 0;
 		num1 = 0;
 		num2 = 0;
 		num3 = 0;
 		p = 0;
 		is_num1 = false;
 		is_num2 = false;
 		is_num3 = false;
 	}
 	void print()
 	{
 		cout << type << " " << num1 << " " << num2 << " " << num3 << endl;
 	}
}If_Id, Id_Ex, Ex_Mem, Mem_Wb;
bool IF (int q)
{
	if (lock_mem)
	{
		lock_mem = false;
		return false;
	}
	If_Id.line = q;
	If_Id.type = program[q].type;
	If_Id.num1 = program[q].num1;
	If_Id.num2 = program[q].num2;
	If_Id.num3 = program[q].num3;
	If_Id.p = program[q].p;
	If_Id.is_num1 = program[q].is_num1;
	If_Id.is_num2 = program[q].is_num2;
	If_Id.is_num3 = program[q].is_num3;
//	cout << If_Id.type << " " << If_Id.num1 << " " << If_Id.num2 << " " << If_Id.num3 << endl;
	return true;
}
bool ID ()
{
	if (If_Id.type == -1)
		return true;
	if (If_Id.type == 47 && lock_reg[get_reg_num("hi")])
		return false;
	else if (If_Id.type == 48 && lock_reg[get_reg_num("lo")])
		return false;
	else if (If_Id.num1 < 100 && lock_reg[If_Id.num1])
		return false;
	if (!If_Id.is_num2 && If_Id.num2 < 100 && lock_reg[If_Id.num2])
	{
//		cout << "!!!" << endl;
		return false;
	}
	if (!If_Id.is_num3 && If_Id.num3 < 100 && lock_reg[If_Id.num3])
		return false;
//	cout << "$$$" << endl;
	Id_Ex = If_Id;
	if (If_Id.type >= 39 && If_Id.type < 46)
	{
		if (If_Id.num2 <= 100)
			Id_Ex.num2 = reg[If_Id.num2];
		if (If_Id.type >= 43)
			Id_Ex.num1 = reg[If_Id.num1];
		else
			lock_reg[Id_Ex.num1] = true;
		pc++;
		return true;
	}
	if (If_Id.type < 22 || (If_Id.type >= 39 && If_Id.type < 43))
	{
		lock_reg[Id_Ex.num1] = true;
		if (If_Id.type >= 5 && If_Id.type < 9 && If_Id.num3 == 0 && !If_Id.is_num3)
		{
			if (lock_reg[get_reg_num("hi")] || lock_reg[get_reg_num("lo")])
			{
				Id_Ex.clear();
				return false;
			}
			lock_reg[Id_Ex.num1] = false;
			Id_Ex.num1 = reg[Id_Ex.num1];
			lock_reg[get_reg_num("hi")] = true;
			lock_reg[get_reg_num("lo")] = true;
		}
	}
	if (If_Id.type == 50)
	{
		if (lock_reg[get_reg_num("a0")] || lock_reg[get_reg_num("v0")])
		{
			Id_Ex.clear();
			return false;
		}
		pc++;
		Id_Ex.num1 = reg[get_reg_num("a0")];
		Id_Ex.num2 = reg[get_reg_num("v0")];
		lock_reg[get_reg_num("v0")] = true;
		return true;
	}
	if (If_Id.type == 22)
	{
		pc = Id_Ex.num1;
		return true;
	}
	else if (If_Id.type > 22 && If_Id.type < 29)
	{
		Id_Ex.num1 = reg[If_Id.num1];
		if (!If_Id.is_num2 && Id_Ex.num2 < 100)
			Id_Ex.num2 = reg[If_Id.num2];
		pc = branch() ? Id_Ex.num3 : pc + 1;
		return true;
	}
	else if (If_Id.type >= 29 && If_Id.type < 35)
	{
		Id_Ex.num1 = reg[If_Id.num1];
		pc = branch() ? Id_Ex.num2 : pc + 1;
		return true;
	}
	else if (If_Id.type >= 35 && If_Id.type < 39)
	{
		if (If_Id.type == 35 || If_Id.type == 37)
			pc = If_Id.num1;
		else
			pc = reg[If_Id.num1];
	}
	else if (If_Id.type == 47 || If_Id.type == 48)
	{
		lock_reg[If_Id.num1] = true;
		pc++;
		return true;
	}
	else if (If_Id.type == 46)
	{
		lock_reg[Id_Ex.num1] = true;
		pc++;
		return true;
	}
	else
		pc++;
	if (!If_Id.is_num2 && Id_Ex.num2 < 100)
		Id_Ex.num2 = reg[If_Id.num2];
	if (!If_Id.is_num3 && Id_Ex.num3 < 100)
		Id_Ex.num3 = reg[If_Id.num3];
//	cout << Id_Ex.type << " " << Id_Ex.num1 << " " << Id_Ex.num2 << " " << Id_Ex.num3 << endl;
//	cout << Id_Ex.type << " " << reg[Id_Ex.num1] << " " << Id_Ex.num2 << " " << Id_Ex.num3 << endl;
	return true;
}
bool Ex()
{
	if (Id_Ex.type == -1)
		return true;
	Ex_Mem = Id_Ex;
	if (Id_Ex.type < 3)
	{
		Ex_Mem.num2 = Id_Ex.num2 + Id_Ex.num3;
	}
	else if (Id_Ex.type < 5)
	{
		Ex_Mem.num2 = Id_Ex.num2 - Id_Ex.num3;
	}
	else if (Id_Ex.type < 7)
	{
		if (Id_Ex.num3 == 0 && !Id_Ex.is_num3)
		{
			long long qqq = Id_Ex.num1 * Id_Ex.num2;
			Ex_Mem.num2 = qqq;
			Ex_Mem.num1 = (qqq >> 32);
		}
		else
		{
			Ex_Mem.num2 = Id_Ex.num2 * Id_Ex.num3;
		}
	}
	else if (Id_Ex.type < 9)
	{
		if (Id_Ex.num3 == 0 && !Id_Ex.is_num3)
		{
			Ex_Mem.num1 = Id_Ex.num1 % Id_Ex.num2;
			Ex_Mem.num2 = Id_Ex.num1 / Id_Ex.num2;
		}
		else
		{
			Ex_Mem.num2 = Id_Ex.num2 / Id_Ex.num3;
		}
	}
	else if (Id_Ex.type < 11)
	{
		Ex_Mem.num2 = Id_Ex.num2 ^ Id_Ex.num3;
	}
	else if (Id_Ex.type < 12)
	{
		Ex_Mem.num2 = -Id_Ex.num2;
	}
	else if (Id_Ex.type < 13)
	{
		Ex_Mem.num2 = ~Id_Ex.num2;
	}
	else if (Id_Ex.type < 15)
	{
		Ex_Mem.num2 = Id_Ex.num2 % Id_Ex.num3;
	}
	else if (Id_Ex.type == 16)
	{
		Ex_Mem.num2 = (Id_Ex.num2 == Id_Ex.num3);
	}
	else if (Id_Ex.type == 17)
	{
		Ex_Mem.num2 = (Id_Ex.num2 >= Id_Ex.num3);
	}
	else if (Id_Ex.type == 18)
	{
		Ex_Mem.num2 = (Id_Ex.num2 > Id_Ex.num3);
	}
	else if (Id_Ex.type == 19)
	{
		Ex_Mem.num2 = (Id_Ex.num2 <= Id_Ex.num3);
	}
	else if (Id_Ex.type == 20)
	{
		Ex_Mem.num2 = (Id_Ex.num2 < Id_Ex.num3);
	}
	else if (Id_Ex.type == 21)
	{
		Ex_Mem.num2 = (Id_Ex.num2 != Id_Ex.num3);
	}
	else if (Id_Ex.type == 21)
	{
		Ex_Mem.num2 = (Id_Ex.num2 != Id_Ex.num3);
	}
	else if (Id_Ex.type == 23)
	{
		if (Id_Ex.num1 == Id_Ex.num2)
//			Ex_Mem.num1 = Id_Ex.num3;
		{
			if (Id_Ex.num3 != pc)
			{
				pc = Id_Ex.num3;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
			
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 24)
	{
		if (Id_Ex.num1 != Id_Ex.num2)
		{
			if (Id_Ex.num3 != pc)
			{
				pc = Id_Ex.num3;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 25)
	{
		if (Id_Ex.num1 >= Id_Ex.num2)
		{
			if (Id_Ex.num3 != pc)
			{
				pc = Id_Ex.num3;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 26)
	{
		if (Id_Ex.num1 <= Id_Ex.num2)
		{
			if (Id_Ex.num3 != pc)
			{
				pc = Id_Ex.num3;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 27)
	{
		if (Id_Ex.num1 > Id_Ex.num2)
		{
			if (Id_Ex.num3 != pc)
			{
				pc = Id_Ex.num3;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 28)
	{
		if (Id_Ex.num1 < Id_Ex.num2)
		{
			if (Id_Ex.num3 != pc)
			{
				pc = Id_Ex.num3;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 29)
	{
		if (Id_Ex.num1 == 0)
		{
			if (Id_Ex.num2 != pc)
			{
				pc = Id_Ex.num2;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 30)
	{
		if (Id_Ex.num1 != 0)
		{
			if (Id_Ex.num2 != pc)
			{
				pc = Id_Ex.num2;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 31)
	{
		if (Id_Ex.num1 <= 0)
		{
			if (Id_Ex.num2 != pc)
			{
				pc = Id_Ex.num2;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 32)
	{
		if (Id_Ex.num1 >= 0)
		{
			if (Id_Ex.num2 != pc)
			{
				pc = Id_Ex.num2;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 33)
	{
		if (Id_Ex.num1 > 0)
		{
			if (Id_Ex.num2 != pc)
			{
				pc = Id_Ex.num2;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type == 34)
	{
		if (Id_Ex.num1 > 0)
		{
			if (Id_Ex.num2 != pc)
			{
				pc = Id_Ex.num2;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(true);
			return true;
		}
		else
//			Ex_Mem.num1 = -1;
		{
			if (pc != Id_Ex.line + 1)
			{
				pc = Id_Ex.line + 1;
				Id_Ex.clear();
				If_Id.clear();
			}
			change_branch(false);
		}
	}
	else if (Id_Ex.type >= 39 && Id_Ex.type < 46)
	{
		if (Id_Ex.num2 < 100)
			Ex_Mem.num2 = reg[Id_Ex.num2];
		Ex_Mem.num2 = Ex_Mem.num2 + Id_Ex.p;
	}
	else if (Id_Ex.type == 47)
	{
		Ex_Mem.num2 = reg[get_reg_num("hi")];
	}
	else if (Id_Ex.type == 48)
	{
		Ex_Mem.num2 = reg[get_reg_num("lo")];
	}
	return true;
}
bool Mem()
{
	if (Ex_Mem.type == -1)
		return true;
	Mem_Wb = Ex_Mem;
	lock_mem = true;
	if (Ex_Mem.type >= 40 && Ex_Mem.type < 46)
	{
		if (Ex_Mem.type < 43)
		{
//			cout << Ex_Mem.num2 << endl;
			Mem_Wb.num2 = load (Ex_Mem.num2, 1 << (Ex_Mem.type - 40));
		}
		else
		{
			store (Mem_Wb.num1, Ex_Mem.num2, 1 << (Ex_Mem.type - 43));
		}
	}
	else if (Ex_Mem.type == 50)
	{
		system_call(Ex_Mem.num1, Ex_Mem.num2);
	}
	else
	{
		lock_mem = false;
	}
	return true;
}
bool WB()
{
	if (Mem_Wb.type == -1)
		return false;
//	cout << Mem_Wb.line << " " << pm[Mem_Wb.line] << endl;
	if (Mem_Wb.type == 50)
	{
//		pc++;
		lock_reg[get_reg_num("v0")] = false;
//		cout << program[Mem_Wb.line].type << " " << reg[get_reg_num("a0")] << " " << reg[get_reg_num("v0")] << endl;
		return true;
	}
	else if (Mem_Wb.type == 46)
	{
		lock_reg[Mem_Wb.num1] = false;
		reg[Mem_Wb.num1] = reg[Mem_Wb.num2];
		return true;
	}
//	cout << program[Mem_Wb.line].type << " " << program[Mem_Wb.line].num1 << " " << program[Mem_Wb.line].num2 << endl;
//	cout << Mem_Wb.type << " " << Mem_Wb.num1 << " " << Mem_Wb.num2 << endl;
	if (Mem_Wb.type < 22 || (Mem_Wb.type >= 39 && Mem_Wb.type < 43) || (Mem_Wb.type > 46 && Mem_Wb.type < 49))
	{
		if (Mem_Wb.type>= 5 && Mem_Wb.type < 9)
		{
			if (lock_reg[get_reg_num("hi")])
			{
				reg[get_reg_num("hi")] = Mem_Wb.num1;
				reg[get_reg_num("lo")] = Mem_Wb.num2;
				lock_reg[get_reg_num("hi")] = false;
				lock_reg[get_reg_num("lo")] = false;
			}
			else
			{
				reg[Mem_Wb.num1] = Mem_Wb.num2;
				lock_reg[Mem_Wb.num1] = false;
			}
		}
		else
		{
			reg[Mem_Wb.num1] = Mem_Wb.num2;
			lock_reg[Mem_Wb.num1] = false;
		}
	}
	else if (Mem_Wb.type < 39 && Mem_Wb.type >= 37)
		reg[31] = Mem_Wb.line + 1;
/*	if (Mem_Wb.type < 39 && Mem_Wb.type >= 22)
	{
		if (Mem_Wb.type >= 37)
			reg[31] = pc + 1;
		if (Mem_Wb.type == 36 || Mem_Wb.type == 38)
			pc = reg[Mem_Wb.num1];
		else
			pc = Mem_Wb.num1 == -1 ? pc + 1 : Mem_Wb.num1;
	}
	else
		pc++;*/
//	cout << Mem_Wb.type << " " << Mem_Wb.num1 << " " << Mem_Wb.num2 << endl;
	if (Mem_Wb.type < 22 || (Mem_Wb.type >= 39 && Mem_Wb.type < 43))
		lock_reg[Mem_Wb.num1] = false;
	return true;
}
void print()
{
	cout << "WB " << Mem_Wb.line << " " << pm[Mem_Wb.line] << endl;
	cout << "Mem " << Ex_Mem.line << " " << pm[Ex_Mem.line] << endl;
	cout << "Ex " << Id_Ex.line << " " << pm[Id_Ex.line] << endl;
	cout << "Id " << If_Id.line << " " << pm[If_Id.line] << endl;
	cout << "IF " << pc << " " << pm[pc] << endl;
	cout << endl;
}
void work ()
{
	pc = line_label_num["main"];
	If_Id.clear();
	Id_Ex.clear();
	Ex_Mem.clear();
	Mem_Wb.clear();
	while (true)
	{
//		print();
		if (WB())
		{
//			out << Mem_Wb.line << " " << pm[Mem_Wb.line] << endl;
//			if (Mem_Wb.num1 < 100)
//			out << lock_reg[Mem_Wb.num1] << " " << pc << endl;
//			for (int i = 0; i < 35; i++)
//				out << reg_name[i] << ":" << reg[i] << endl;
		}
		Mem_Wb.clear();
		if (Mem())
		{
			Ex_Mem.clear();
			if (Ex())
			{
				Id_Ex.clear();
//				out << If_Id.line << " " << pm[If_Id.line] << endl;
				if (ID())
				{
					If_Id.clear();
//					out << "IF " << pc << " " << pm[pc] << endl;
//					out << program[pc].type << " " << program[pc].num1 << " " << program[pc].num2 << endl;
					IF(pc);
				}
			}
		}
	}
}
void work_test ()
{
	pc = line_label_num["main"];
	while (true)
	{
//		cout << pc << " " << pm[pc] << endl;
		If_Id.clear();
		IF(pc);
		Id_Ex.clear();
		ID();
		Ex_Mem.clear();
		Ex();
		Mem_Wb.clear();
		Mem();
		WB();
		lock_mem = false;
	}
}
void build_program()
{
	for (int i = 0; i < line_num; i++)
	{
		string now = pm[i], op;
		op = now.substr(0, now.find(" "));
		program[i].type = op_type(op);
		now = now.substr(now.find(" ") + 1);
		if (op[0] == 'b' || op == "j" || op == "jal")
		{
			if ((int)now.find(",") != -1)
			{
				string num = now.substr(0, now.find(","));
				now = now.substr(now.find(",") + 1);
				del_space(now);
				del_space(num);
				program[i].is_num1 = false;
				program[i].num1 = string_to_address(num);
				if ((int)now.find(",") != -1)
				{
					num = now.substr(0, now.find(","));
					now = now.substr(now.find(",") + 1);
					del_space(now);
					del_space(num);
					if (is_string_num(num))
					{
						program[i].is_num2 = true;
						program[i].num2 = string_num(num);
					}
					else
					{
						program[i].is_num2 = false;
						program[i].num2 = string_to_address(num);
					}
					num = now;
					del_space(num);
					program[i].is_num3 = false;
					program[i].num3 = line_label_num[num];
				}
				else
				{
					num = now;
					del_space(num);
					program[i].is_num2 = false;
					program[i].num2 = line_label_num[num];
				}
			}
			else
			{
				string num = now;
				del_space(num);
				program[i].is_num1 = false;
				program[i].num1 = line_label_num[num];
			}
		}
		else
		{
			if ((int)now.find(",") != -1)
			{
				string num = now.substr(0, now.find(","));
				now = now.substr(now.find(",") + 1);
				del_space(now);
				del_space(num);
				if (is_string_num(num))
				{
					program[i].is_num1 = true;
					program[i].num1 = string_num(num);
				}
				else
				{
					program[i].is_num1 = false;
					program[i].num1 = string_to_address(num);
				}
				if ((int)now.find(",") != -1)
				{
					num = now.substr(0, now.find(","));
					now = now.substr(now.find(",") + 1);
					del_space(now);
					del_space(num);
					if (is_string_num(num))
					{
						program[i].is_num2 = true;
						program[i].num2 = string_num(num);
					}
					else
					{
						program[i].is_num2 = false;
						program[i].num2 = string_to_address(num);
					}
					num = now;
					del_space(num);
					if (is_string_num(num))
					{
						program[i].is_num3 = true;
						program[i].num3 = string_num(num);
					}
					else
					{
						program[i].is_num3 = false;
						program[i].num3 = string_to_address(num);
					}
				}
				else
				{
					num = now;
					del_space(num);
					if (is_string_num(num))
					{
						program[i].is_num2 = true;
						program[i].num2 = string_num(num);
					}
					else if ((int)num.find("(") == -1)
					{
						program[i].is_num2 = false;
						program[i].num2 = string_to_address(num);
					}
					else
					{
						del_space(now);
						num = now.substr(0, now.find("("));
						program[i].p = string_num(num);
						num = now.substr(now.find("(") + 1);
						num = num.substr(0, num.find(")"));
						program[i].is_num2 = false;
						program[i].num2 = string_to_address(num);
					}
				}
			}
			else
			{
				string num = now;
				del_space(num);
				if (is_string_num(num))
				{
					program[i].is_num1 = true;
					program[i].num1 = string_num(num);
				}
				else
				{
					program[i].is_num1 = false;
					program[i].num1 = string_to_address(num);
				}
			}
		}
	}
}
int main(int argc, char const *argv[])
{
	out.open ("1.out", ios::out);
	Init(argv[1]);
//	Init();
	build_data();
	build_program();
	work();
	return 0;
}